package sda.code.primeoracle;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnitParamsRunner.class)
public class OracleTest {

    private Oracle sut;

    @Before
    public void setup() {
        sut = new Oracle();
    }

    @Test
    @Parameters({
            "2", "3", "5", "7", "11", "13", "17", "19", "23", "29",
            "7907", "7919", "512927357", "982451653", "2147483647"
    })
    public void isPrime(int candidate) {
        assertTrue(sut.proclaim(candidate));
    }

    @Test
    @Parameters({
            "1", "4", "6", "8", "9", "10", "12", "15", "1000000", "1000000000", "2147483645"
    })
    public void notPrime(int candidate) {
        assertFalse(sut.proclaim(candidate));
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters({
            "0", "-1", "-2", "-7", "-4099", "-2147483648"
    })
    public void errors(int candidate) {
        sut.proclaim(candidate);
    }
}
