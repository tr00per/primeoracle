package sda.code.primeoracle;

import org.junit.Assert;
import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class AppFPTest {
    private static final Supplier<String> ASSERT_NO_CALL_TO_SUPPLIER = () -> {
        fail("Unexpected call to line supplier");
        return null; // won't be reached
    };
    private static final Predicate<Integer> ASSERT_NO_CALL_TO_LOGIC = i -> {
        fail("Unexpected call to logic");
        return false; // won't be reached
    };
    private static final Consumer<Boolean> ASSERT_NO_CONTINUATION = b -> fail("Continuation called");

    @Test
    public void happyPathPrime() {
        new AppFP(() -> "7", new Oracle()::proclaim).run(Assert::assertTrue);
    }

    @Test
    public void happyPathNonPrime() {
        new AppFP(() -> "12", new Oracle()::proclaim).run(Assert::assertFalse);
    }

    @Test(expected = NoSuchElementException.class)
    public void noNextLine() {
        new AppFP(() -> {
            throw new NoSuchElementException();
        }, ASSERT_NO_CALL_TO_LOGIC).run(ASSERT_NO_CONTINUATION);
    }

    @Test(expected = IllegalStateException.class)
    public void scannerClosed() {
        new AppFP(() -> {
            throw new IllegalStateException();
        }, ASSERT_NO_CALL_TO_LOGIC).run(ASSERT_NO_CONTINUATION);
    }

    @Test
    public void emptyLine() {
        new AppFP(() -> "", ASSERT_NO_CALL_TO_LOGIC).run(ASSERT_NO_CONTINUATION);
    }

    @Test
    public void numberPrecededWithSpace() {
        new AppFP(() -> " 7", i -> {
            assertEquals(7L, (long) i);
            return true;
        }).run(Assert::assertTrue);
    }

    @Test
    public void numberSucceededWithSpace() {
        new AppFP(() -> "7 ", i -> {
            assertEquals(7L, (long) i);
            return true;
        }).run(Assert::assertTrue);
    }

    @Test
    public void numberPrecededWithTab() {
        new AppFP(() -> "\t12", i -> {
            assertEquals(12L, (long) i);
            return false;
        }).run(Assert::assertFalse);
    }

    @Test
    public void numberSucceededWithTab() {
        new AppFP(() -> "12\t", i -> {
            assertEquals(12L, (long) i);
            return false;
        }).run(Assert::assertFalse);
    }

    @Test(expected = NumberFormatException.class)
    public void failOnHexNumber() {
        new AppFP(() -> "0xCAFE", ASSERT_NO_CALL_TO_LOGIC).run(ASSERT_NO_CONTINUATION);
    }

    @Test(expected = NumberFormatException.class)
    public void numberTooLarge() {
        new AppFP(() -> "999999999999999999999", ASSERT_NO_CALL_TO_LOGIC).run(ASSERT_NO_CONTINUATION);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullConsumer() {
        new AppFP(ASSERT_NO_CALL_TO_SUPPLIER, ASSERT_NO_CALL_TO_LOGIC).run(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullScanner() {
        new AppFP(null, ASSERT_NO_CALL_TO_LOGIC).run(ASSERT_NO_CONTINUATION);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullOracle() {
        new AppFP(ASSERT_NO_CALL_TO_SUPPLIER, null).run(ASSERT_NO_CONTINUATION);
    }

    @Test(expected = IllegalArgumentException.class)
    public void oracleFails() {
        new AppFP(() -> "0", i -> {
            assertEquals(0L, (long) i);
            throw new IllegalArgumentException();
        }).run(ASSERT_NO_CONTINUATION);
    }

}
