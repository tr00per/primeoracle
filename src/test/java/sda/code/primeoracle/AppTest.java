package sda.code.primeoracle;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.function.Consumer;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

public class AppTest {
    private static final Consumer<Boolean> ASSERT_NO_CONTINUATION = b -> fail("Continuation called");

    private Oracle oracleMock;
    private Scanner scannerMock;
    private App sut;

    @Before
    public void setup() throws Exception {
        scannerMock = mock(Scanner.class);
        oracleMock = mock(Oracle.class);
        sut = new App(scannerMock, oracleMock);
    }

    @Test
    public void happyPathPrime() {
        sut = new App(scannerMock, new Oracle());
        when(scannerMock.nextLine()).thenReturn("7");
        sut.run(Assert::assertTrue);
    }

    @Test
    public void happyPathNonPrime() {
        sut = new App(scannerMock, new Oracle());
        when(scannerMock.nextLine()).thenReturn("12");
        sut.run(Assert::assertFalse);
    }

    @Test(expected = NoSuchElementException.class)
    public void noNextLine() {
        when(scannerMock.nextLine()).thenThrow(NoSuchElementException.class);
        sut.run(ASSERT_NO_CONTINUATION);
    }

    @Test(expected = IllegalStateException.class)
    public void scannerClosed() {
        when(scannerMock.nextLine()).thenThrow(IllegalStateException.class);
        sut.run(ASSERT_NO_CONTINUATION);
    }

    @Test
    public void emptyLine() {
        when(scannerMock.nextLine()).thenReturn("");
        sut.run(ASSERT_NO_CONTINUATION);
    }

    @Test
    public void numberPrecededWithSpace() {
        when(scannerMock.nextLine()).thenReturn(" 7");
        when(oracleMock.proclaim(7)).thenReturn(true);
        sut.run(Assert::assertTrue);
        verify(oracleMock).proclaim(7);
    }

    @Test
    public void numberSucceededWithSpace() {
        when(scannerMock.nextLine()).thenReturn("7 ");
        when(oracleMock.proclaim(7)).thenReturn(true);
        sut.run(Assert::assertTrue);
        verify(oracleMock).proclaim(7);
    }

    @Test
    public void numberPrecededWithTab() {
        when(scannerMock.nextLine()).thenReturn("\t12");
        when(oracleMock.proclaim(12)).thenReturn(false);
        sut.run(Assert::assertFalse);
        verify(oracleMock).proclaim(12);
    }

    @Test
    public void numberSucceededWithTab() {
        when(scannerMock.nextLine()).thenReturn("12\t");
        when(oracleMock.proclaim(12)).thenReturn(false);
        sut.run(Assert::assertFalse);
        verify(oracleMock).proclaim(12);
    }

    @Test(expected = NumberFormatException.class)
    public void failOnHexNumber() {
        when(scannerMock.nextLine()).thenReturn("0xCAFE");
        sut.run(ASSERT_NO_CONTINUATION);
        verify(scannerMock).nextLine();
        verify(oracleMock, never()).proclaim(anyInt());
    }

    @Test(expected = NumberFormatException.class)
    public void numberTooLarge() {
        when(scannerMock.nextLine()).thenReturn("999999999999999999999");
        sut.run(ASSERT_NO_CONTINUATION);
        verify(scannerMock).nextLine();
        verify(oracleMock, never()).proclaim(anyInt());
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullConsumer() {
        sut.run(null);
        verify(scannerMock, never()).nextLine();
        verify(oracleMock, never()).proclaim(anyInt());
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullScanner() {
        new App(null, oracleMock);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullOracle() {
        new App(scannerMock, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void oracleFails() {
        when(scannerMock.nextLine()).thenReturn("0");
        when(oracleMock.proclaim(anyInt())).thenThrow(IllegalArgumentException.class);
        sut.run(ASSERT_NO_CONTINUATION);
    }

}
