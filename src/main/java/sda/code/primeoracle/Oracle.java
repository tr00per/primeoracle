package sda.code.primeoracle;

public class Oracle {
    public boolean proclaim(int number) {
        if (number < 1) {
            throw new IllegalArgumentException("Only natural numbers are allowed");
        }
        if (number == 2) {
            return true;
        }
        if (number == 1 || number % 2 == 0) {
            return false;
        }
        double sqrt = Math.sqrt(number);
        for (int i = 3; i <= sqrt; i += 2) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
