package sda.code.primeoracle;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            Oracle oracle = new Oracle();

            System.out.print("(1) Give me a natural number, and I shall proclaim it's nature: ");
            new App(scanner, oracle).run(r -> System.out.println("It's a prime? " + r));

            System.out.print("(2) Give me a natural number, and I shall proclaim it's nature: ");
            new AppFP(scanner::nextLine, oracle::proclaim).run(r -> System.out.println("It's a prime? " + r));
        }
    }
}
