package sda.code.primeoracle;

import java.util.Scanner;
import java.util.function.Consumer;

public class App {
    private final Scanner scanner;
    private final Oracle oracle;

    public App(Scanner scanner, Oracle oracle) {
        if (scanner == null || oracle == null) {
            throw new IllegalArgumentException("Dependencies cannot be null");
        }
        this.scanner = scanner;
        this.oracle = oracle;
    }

    public void run(Consumer<Boolean> next) {
        if (next == null) {
            throw new IllegalArgumentException("Continuation cannot be null");
        }
        String line = scanner.nextLine();
        if (line.isEmpty()) {
            return;
        }
        String trimmed = line.replaceAll("\\s+", "");
        int number = Integer.parseInt(trimmed);
        if (number < 0) {
            return;
        }
        boolean verdict = oracle.proclaim(number);
        next.accept(verdict);
    }
}
