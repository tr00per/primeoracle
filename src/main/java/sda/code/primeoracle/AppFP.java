package sda.code.primeoracle;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class AppFP {
    private final Supplier<String> nextLine;
    private final Predicate<Integer> isPrime;

    public AppFP(Supplier<String> nextLine, Predicate<Integer> isPrime) {
        if (nextLine == null || isPrime == null) {
            throw new IllegalArgumentException("Dependencies cannot be null");
        }
        this.nextLine = nextLine;
        this.isPrime = isPrime;
    }

    public void run(Consumer<Boolean> next) {
        if (next == null) {
            throw new IllegalArgumentException("Continuation cannot be null");
        }
        String line = nextLine.get();
        if (line.isEmpty()) {
            return;
        }
        String trimmed = line.replaceAll("\\s+", "");
        int number = Integer.parseInt(trimmed);
        if (number < 0) {
            return;
        }
        boolean verdict = isPrime.test(number);
        next.accept(verdict);
    }
}
